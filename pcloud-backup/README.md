# Home Assistant Add-on: PCloud Backup

Europe's most secure cloud storage Store, share and access all your files using one simple and highly secure platform, anytime and anywhere you go.
https://www.pcloud.com/eu

## Installation

Follow these steps to get the add-on installed on your system:

1. Enable **Advanced Mode** in your Home Assistant user profile.
2. Navigate in your Home Assistant frontend to **Supervisor** -> **Add-on Store**.
3. Search for "PCloud Backup" add-on and click on it.
4. Click on the "INSTALL" button.

## How to use

1. Set the `pcloud_username`, and `pcloud_password`. 
2. Optionally / if necessary, change `pcloud_region`, `pcloud_path`, and `delete_local_backups` and `local_backups_to_keep` configuration options.
3. Start the add-on to sync the `/backup/` directory to the configured `pcloud_path` Folder in your PCloud Account. You can also automate this of course, see example below:

## Automation

To automate your backup creation and syncing to PCloud, add these two automations in Home Assistants `configuration.yaml` and change it to your needs:
```
automation:
  # create a full backup
  - id: backup_create_full_backup
    alias: Create a full backup every day at 4am
    trigger:
      platform: time
      at: "04:00:00"
    action:
      service: hassio.backup_full
      data:
        # uses the 'now' object of the trigger to create a more user friendly name (e.g.: '202101010400_automated-backup')
        name: "{{as_timestamp(trigger.now)|timestamp_custom('%Y%m%d%H%M', true)}}_automated-backup"

  # Starts the addon 15 minutes after every hour to make sure it syncs all backups, also manual ones, as soon as possible
  - id: backup_upload_to_pcloud
    alias: Upload to PCloud
    trigger:
      platform: time_pattern
      # Matches every hour at 15 minutes past every hour
      minutes: 15
    action:
      service: hassio.addon_start
      data:
      addon: XXXXX_pcloud-backup
```

The automation above first creates a full backup at 4am, and then at 4:15am syncs to your PCloud and if configured deletes local backups according to your configuration.

## Configuration

Example add-on configuration:

```
pcloud_username: mail@example.com
pcloud_password: XXXXXXXXXXXXXXXXXX
pcloud_region: eu
pcloud_path: homeassistant/backup
delete_local_backups: true
local_backups_to_keep: 3
```

### Option: `pcloud_username` (required)
Your PCloud email for login.

### Option: `pcloud_password` (required)
Your PCloud password for login.

### Option: `pcloud_region` (optional, Default: eu)
See https://www.pcloud.com/de/data-regions/ for all available regions.

### Option: `pcloud_path` (optional, Default: homeassistant/backup)
Remote path to store the backups.

### Option: `delete_local_backups` (optional, Default: true)
Should the addon remove oldest local backups after syncing to your PCloud Storage? You can configure how many local backups you want to keep with the Option `local_backups_to_keep`. Oldest Backups will get deleted first.

### Option: `local_backups_to_keep` (optional, Default: 3)
How many backups you want to keep locally? If you want to disable automatic local cleanup, set `delete_local_backups` to false.

## Thanks
This addon is highly inspired by https://raw.githubusercontent.com/thomasfr/hass-addons