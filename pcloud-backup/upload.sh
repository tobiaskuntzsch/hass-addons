#!/bin/bash

folder_id=$1
monitor_path=$2

files=$(cd $monitor_path && /usr/bin/find . -maxdepth 1 -type f -not -path '*/\.*' | /usr/bin/sed 's/^\.\///g' )
for entry in $files
do
    file_id=$(/root/.cargo/bin/pcloud-cli folder $folder_id list | /usr/bin/grep $entry | /usr/bin/awk '{print $1}')
    echo "File name ${monitor_path}/${entry}"
    if [ -n "$file_id" ] && [ $file_id -gt 0 ]; then
        echo "File exist"
    else
        file_id=$(/root/.cargo/bin/pcloud-cli file upload $monitor_path/$entry | /usr/bin/awk '{print $NF}')
        echo "Upload file"    
        /root/.cargo/bin/pcloud-cli file move $file_id $folder_id >/dev/null 2>&1
    fi
    echo "File ID is ${file_id}"
    echo "******************************"
done